<?php
  
  include_once("conexao.php");
  @session_start();

  $cod = $_SESSION['cod'];
  $sql = "select * from tbl_cadastro_pessoa WHERE cod='$cod'";
  $consulta = mysqli_query($conexao,$sql);
  $registros = mysqli_num_rows($consulta);

   if (!isset($_SESSION['nome']) && !isset($_SESSION['email']) && !isset($_SESSION['cod'])) {

    header('Location: editar_perfil.php');
                
          exit;  
  }

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Editar Perfil</title>
	<link rel="stylesheet" type="text/css" href="css/cadastro.css">

</head>
<body>
  <form method="POST" action="config_perfil.php">

  		<div class="horinzontal">

  			<img src="css/imagens/logo_marca.png"  width="100px" id="logo">
  			<div id="sistema"><br> SISTEMA ANIMAL</div>
  			
  		</div>
				<div class="imag"><br><img src="css/imagens/foto.png" width="70px"> <br><input class="botao anex"type="submit" name="imagem" value="Inserir imagem"></div>
				

		<table>
			<caption>Editar Perfil</caption>
			<thead>
				
	<?php
    
    while($exibirRegistros = mysqli_fetch_array($consulta)) {

          $cod = $exibirRegistros[0];
          $matricula = $exibirRegistros[1];
          $rg = $exibirRegistros[2];
          $cpf = $exibirRegistros[3];
          $email = $exibirRegistros[4];
          $senha = $exibirRegistros[5];
          $nasc = $exibirRegistros[6];
          $sexo = $exibirRegistros[7];
          $end = $exibirRegistros[8];
          $nome = $exibirRegistros[9];
          $titulacao = $exibirRegistros[10];

            echo "<tbody>";

            echo "<tr>";

              echo "<td><label for='Nome'>*Nome</label> <input type='text' value='$nome' name='nome' id='Nome' maxlength='100' class='campo' required pattern='^[^-\s][a-zA-ZÀ-ú ]*{2,100}$'></td>";
              echo "<td><label for='rg'>*RG </label><input type='text' value='$rg' name='rg' maxlength='13' pattern='.{13,14}' class='campo' required pattern='[0-9]+$' placeholder='apenas numeros' ></td>";
              echo "<td><label for='Cpf'>*CPF </label> <input type='text' value='$cpf' name='cpf' maxlength='11' pattern='.{11,12}' class='campo' required pattern='[0-9]+$' placeholder='apenas numeros' ></td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td><label for='Matri'>*Matrícula </label> <input type='text' value='$matricula' name='matricula'id='Matri' maxlength='10' pattern='.{10,11}' class='campo' required pattern='[0-9]+$' placeholder='apenas numeros'></td>";
              echo "<td>*Data de Nascimento <input type='date' name='nasc' id='cNasc' class='campo'  value='$nasc' required pattern='[0-9]{2}\/[0-9]{2}\/[0-9]{4}$'></td>";
                if ($sexo == "Feminino") {
                  echo "<td>*Sexo<br><input type='radio' name='sexo' value='$sexo' required><label>Masculino</label><br>
                   <input type='radio' name='sexo' value='$sexo' checked='true'  required><label>Feminino</label>
                   <br><input type='radio' name='sexo' value='Outro' required><label>Outro</label></td></td>";
                }else{
              echo "<td>*Sexo<br><input type='radio' name='sexo' value='$sexo' required checked='true'><label>Masculino</label><br><input type='radio' name='sexo' value='$sexo'required><label>Feminino</label><br><input type='radio' name='sexo' value='Outro' required><label>Outro</label></td></td>";}
            echo "</tr>";
            echo "<tr>";
              echo "<td><label for='cEnd'>*Endereço </label><input type='text' value='$end' name='end' id='cEnd' maxlength='150' class='campo' required></td>";
              echo "<td><label for='cEmail'>*E-mail </label><input type='email' value='$email' name='email'
                maxlength='80' class='campo' required></td>";
              echo "<td><br><label for='cSenha'>*Senha </label><input type='password' value='$senha' name='senha'pattern='.{8,20}' maxlength='20'  class='campo' required><span class='style1'><br>*Senha com no mínino 8 caracteres</span></td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>*Titulação <select name='titulacao' class='campo' required>
				<option value='Doutor'>Doutor</option>
				<option value='Livre-Docente'>Livre-Docente</option>
				<option value='Assistente'>Assistente</option>
				<option value='Titular'>Titular</option>
				<option value='Presidente'>Presidente</option>
				<option value='Vice-Presidente'>Vice-Presidente</option>       
				</select></td>";
            echo "</tr>";
          
          echo "</tbody>";

    }
  mysqli_close($conexao); 
  ?>
   <td><a href="config_perfil.php"><input class="botao salv" type="submit" name="Salvar" value="Salvar"></a> 
	<a href="perfil.php"><input class="botao canc" type="button" name="Cancelar" value="Cancelar"></a></td>
	
	</thead>
  </table>

 </form>

</body>
</html>