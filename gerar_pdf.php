<?php	
include_once("conexao.php");
@session_start();
  $nome = $_SESSION['nome'];
  $protocolo = filter_input(INPUT_GET, 'protocolo');

  $sql = "SELECT * FROM tbl_experimento WHERE protocolo='$protocolo'";
  $consulta = mysqli_query($conexao,$sql);
  $registros = mysqli_num_rows($consulta);

  while($exibirRegistros = mysqli_fetch_array($consulta)) {

          $protocolo = $exibirRegistros[0];
          $especie = $exibirRegistros[1];
          $origem = $exibirRegistros[2];
          $resumo = $exibirRegistros[3];
          $inicio = $exibirRegistros[4];
          $termino = $exibirRegistros[5];
          $quantidade = $exibirRegistros[6];
          $justificar_decisao = $exibirRegistros[7];
          $cod = $exibirRegistros[8];
          $nome = $exibirRegistros[9];
}

	//referenciar o DomPDF com namespace
	use Dompdf\Dompdf;

	// include autoloader
	require_once("dompdf/autoload.inc.php");

	//Criando a Instancia
	$dompdf = new DOMPDF();

	// Carrega seu HTML
	$dompdf->load_html('

		<link rel="stylesheet" type="text/css" href="css/gerar_pdf.css">
		 <div class="horinzontal">

            <img src="css/imagens/logo_marca.png"  width="100px" id="logo">
            <div id="sistema"><br> SISTEMA ANIMAL</div>
            
          </div>


            <div id="legenda">Protocolo</div>
			<table>
			<tbody>
			<tr>
			<td>Codigo:'.$cod.'</td>
			<td><p>Pesquisador Responsavel:</p>'.$nome.'</td>
			<td>Protocolo: '.$protocolo.'</td>
			</tr>
			<tr>
            <td>Justificativa Para Uso:'.$justificar_decisao.'</td>
            <td>Resumo: '.$resumo.'</td>
            <td>Data de Inicio:'.$inicio.'</td>
   			</tr>
   			<tr>
            <td>Data de Termino:'.$termino.'</td>
            <td>Especie:'.$especie.'</td>
            <td>Origem:'.$origem.'</td>
            <td>Quantidade:'.$quantidade.'</td>
            </tr>
            </tbody> 
            </table>

            <div class="footer" align="center">
<br><br><br><br> ___________________________________<br>
            				Presidente
            </div>	
');

	//Renderizar o html
	$dompdf->render();

	//Exibibir a página
	$dompdf->stream(
		"protocolo_experimento.pdf", 
		array(
			"Attachment" => false //Para realizar o download somente alterar para true
		)
	);
?>