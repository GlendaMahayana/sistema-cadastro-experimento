<?php
  
include_once("conexao.php");
  @session_start();
  $nome = $_SESSION['nome'];
  $protocolo = filter_input(INPUT_GET, 'protocolo');

  $sql = "SELECT * FROM tbl_experimento WHERE protocolo='$protocolo'";
  $consulta = mysqli_query($conexao,$sql);
  $registros = mysqli_num_rows($consulta);

   $email = $_SESSION['email'];
  $nome = $_SESSION['nome'];

  if (!isset($_SESSION['nome']) && !isset($_SESSION['email']) && !isset($_SESSION['cod'])) {

    header('Location: perfil.php');
                
          exit;  
  }

?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <title>Protocolo</title>
  <link rel="stylesheet" type="text/css" href="css/perfil.css">

</head>

<body>
  <div class="horinzontal">

            <img src="css/imagens/logo_marca.png"  width="100px" id="logo">
            <div id="sistema"><br> SISTEMA ANIMAL</div>
            
          </div>


            <div id="legenda">Protocolo</div>
        <table>

          <thead>
                
        
  <?php
    

    while($exibirRegistros = mysqli_fetch_array($consulta)) {

          $protocolo = $exibirRegistros[0];
          $especie = $exibirRegistros[1];
          $origem = $exibirRegistros[2];
          $resumo = $exibirRegistros[3];
          $inicio = $exibirRegistros[4];
          $termino = $exibirRegistros[5];
          $quantidade = $exibirRegistros[6];
          $justificar_decisao = $exibirRegistros[7];
          $cod = $exibirRegistros[8];
          $nome = $exibirRegistros[9];

            echo "<tbody>";

            echo "<tr>";
              echo "<td>Codigo:&emsp;$cod</td>";
              echo "<td>Pesquisador Responsavel:&emsp;$nome</td>";
              echo "<td>Protocolo:&emsp;$protocolo</td>";
              
            echo "</tr>";

            echo "<tr>";

              echo "<td>Justificativa Para Uso:<br>$justificar_decisao </td>";
              echo "<td>Resumo:<br>$resumo </td>";
              echo "<td>Data de Inicio:&emsp;$inicio </td>";
              echo "<td>Data de Termino:&emsp;$termino</td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>Especie:&emsp;$especie</td>";
              echo "<td>Origem:&emsp;$origem</td></td>";
              echo "<td>Quantidade:&emsp;$quantidade</td>";
            echo "</tr>";
              
          echo "</tbody>";

    }
  mysqli_close($conexao); 


  ?>
          </tr>
      </thead>

      </table>
    <a href="protocolo_teste.php"><input class="botao canc" type="button" name="voltar" value="Voltar"></a>
    <?php echo "<a href='gerar_pdf.php?protocolo=$protocolo'><input class='botao canc' type='button' name='' value='Gerar PDF'></a>"; ?>
    <?php echo "<a href='baixar_pdf.php?protocolo=$protocolo'><input class='botao canc' type='button' name='' value='Baixar PDF'></a>"; ?>

    <div class="footer" align="right">Desenvolvido por Glenda Mahayana</div>
</body>
</html>