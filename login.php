<?php
include_once("conexao.php");
@session_start();
 
?>

<!DOCTYPE HTML>
<html lang="pt-br">

<head>

	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/login.css">
	<meta charset="utf-8" />

</head>

<body>
<div class = "login">
    <section class="login-form">
  
      <form method="post" action=""> 

      <div class="row">
      <img src="css/imagens/logo.png" width="20px"><label>*E-mail:</label>
      <input type="text" name="email" class="campo"><p></p>
      </div>
      
       <div class="row">
      <img src="css/imagens/chave.png" width="20px"><label>*Senha:</label>
      <input type="password" name="senha" class="campo" min="8" pattern=".{8,20}" >
       </div>
      <br>
      <div class="row">
      <input class="botao" type="submit" name="entrar" value="login">
      <a href="cadastro.php"><input class="botao cad" type="button" name="cadastro" value="Cadastrar-se"></a>
      <a href="recuperar_senha.php"><input class="botao sen" type="button" name="recupera_senha" value="Recuperar senha"></a>
      </div>
      
      </form>

      <?php

    if(isset($_POST['entrar']) && $_POST['entrar'] == "login"){

        $email = $_POST['email'];
        $senha = $_POST['senha'];

        if (empty($email) || empty($senha)) {

            echo "<br><span class='style1'>*preencha todos os campos</span>";
        }else{

            $query = "SELECT cod, nome, email, senha FROM tbl_cadastro_pessoa WHERE email = '$email' AND senha = '$senha'";
            $result = mysqli_query($conexao,$query);
            $busca = mysqli_num_rows($result);
            $linha = mysqli_fetch_assoc($result);

            if ($busca > 0) {

                $_SESSION['nome'] = $linha['nome'];
                $_SESSION['email'] = $linha['email'];
                $_SESSION['cod'] = $linha['cod'];
                header('Location: menu.php');
                exit;
            }else{

                echo "<span class='style1'>*usuario ou senha inválidos</span>";

            }
          
        }
    }

  ?>

   </section>

  </div>

  <div class="footer">Desenvolvido por Glenda Mahayana</div>

</body>

</html>