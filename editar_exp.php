<?php
@session_start();
   include_once("conexao.php");
  $protocolo = filter_input(INPUT_GET, 'protocolo');

  $sql = "SELECT * FROM tbl_experimento WHERE protocolo='$protocolo'";
  $consulta = mysqli_query($conexao,$sql);
  $registros = mysqli_num_rows($consulta);

   $email = $_SESSION['email'];
  $nome = $_SESSION['nome'];

  if (!isset($_SESSION['nome']) && !isset($_SESSION['email']) && !isset($_SESSION['cod'])) {

    header('Location: perfil.php');
                
          exit;  
  }


?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8">
  <title>Protocolo</title>
  <link rel="stylesheet" type="text/css" href="css/cadastro_exp.css">
 
</head>

<body>

    <form method="GET" action="config_exp.php">

          <div class="horinzontal">

            <img src="css/imagens/logo_marca.png"  width="100px" id="logo">
            <div id="sistema"><br> SISTEMA ANIMAL</div>
            
          </div>

            <div id="legenda">EDITAR PROTOCOLO</div>
          <table>
              
          <thead>
  <?php
                
    while($exibirRegistros = mysqli_fetch_array($consulta)) {

          $protocolo = $exibirRegistros[0];
          $especie = $exibirRegistros[1];
          $origem = $exibirRegistros[2];
          $resumo = $exibirRegistros[3];
          $inicio = $exibirRegistros[4];
          $termino = $exibirRegistros[5];
          $quantidade = $exibirRegistros[6];
          $justificar_decisao = $exibirRegistros[7];
          $cod = $exibirRegistros[8];

          echo "<tbody>";

            echo "<tr>";
            echo "<input type='hidden' name='protocolo' value='$protocolo'>";
              echo "<td>*Justificativa para uso<textarea name='justificar_decisao' class='campo' required='required'>$justificar_decisao</textarea><a href=''><input class='botao anex' type='submit' name='Anexar' value='Anexar arquivo'></a></td>";
              echo "<td>*Resumo<textarea name='resumo' class='campo' value='' required='required'>$resumo</textarea><a href=''><input class='botao anex' type='submit' name='Anexar' value='Anexar arquivo'></a></td>";
              echo "<td>*Data de Inicio <input type='date' name='inicio' id='Inicio' class='campo' value='$inicio' required='required' maxlength='10' pattern='[0-9]{2}\/[0-9]{2}\/[0-9]{4}$' min='2018-06-01'></td>";
              echo "<td>*Data de Término <input type='date' name='termino' id='Término' class='campo' value='$termino' required='required' maxlength='10' pattern='[0-9]{2}\/[0-9]{2}\/[0-9]{4}$' min='2018-06-01'></td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td>*Especie<select name='especie' class='campo' value='$especie' required='required'>
            <option value='Rato'>Rato</option>
            <option value='Coelho'>Coelho</option>
            <option value='Porquinho-da-índia'>Porquinho-da-índia</option>
            <option value='Cão'>Cão</option>
            <option value='Rã'>Rã</option>
            <option value='Peixe'>Peixe</option>
            </select></td>";
              echo "<td>*Origem<select name='origem' class='campo' value='$origem' required='required'>
            <option value='Bioterio1'>Bioterio1</option>
            <option value='Bioterio2'>Bioterio2</option>
            <option value='Bioterio3'>Bioterio3</option>
            <option value='Bioterio4'>Bioterio4</option>
            <option value='Bioterio5'>Bioterio5</option>
            <option value='Bioterio6'>Bioterio6</option>
            </select></td>";
              echo "<td>*Quantidade<select name='quantidade' class='campo' value='$quantidade' required='required'>
            <option value='1'>1</option>
            <option value='2'>2</option>
            <option value='3'>3</option>
            <option value='4'>4</option>
            <option value='5'>5</option>
            <option value='6'>6</option>
            </select></td>";
            echo "</tr>";
            echo "<tr>";
              echo "<td></td>";
            
          
        echo "</tbody>";

    }

      mysqli_close($conexao);
  ?>  
        <td><a href="config_exp.php"><input class="botao prot" type="submit" name="atualizar" value="Atualizar"></a>
        <a href="protocolo_teste.php"><input class="botao prot" type="button" name="Cancelar" value="Cancelar"></a></td>"
     </tr>           
        </thead>
      </table>
    </form>

    <div class="footer" align="right">Desenvolvido por Glenda Mahayana</div>
</body>
</html>