﻿<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cadastro</title>
	<link rel="stylesheet" type="text/css" href="css/cadastro.css">
</head>
<body>
  <form method="post" action="cadastrar.php">

  		<div class="horinzontal">

  			<img src="css/imagens/logo_marca.png"  width="100px" id="logo">
  			<div id="sistema"><br> SISTEMA ANIMAL</div>
  			
  		</div>
				<div class="imag"><br><img src="css/imagens/foto.png" width="70px"> <br><input class="botao anex"type="submit" name="imagem" value="Inserir imagem"></div>
				

		<table>
			<caption>Cadastro de Usuario</caption>
			<thead>
				<tr>
					<td><label for="Nome">*Nome</label> <input type="text" name="nome" maxlength="100" class="campo" required  pattern="^[^-\s][a-zA-ZÀ-ú ]*{2,100}$"></td>
					<td><label for="rg">*RG </label><input type="text" name="rg" class="campo" required maxlength="13" pattern=".{13,14}" placeholder="apenas numeros" pattern='[0-9]+$' ></td>
					<td><label for="Cpf">*CPF</label> <input type="tex" name="cpf" class="campo" required maxlength="11" pattern=".{11,12}" placeholder="apenas numeros" pattern='[0-9]+$' pattern=".{11,11}" ></td>
				</tr>

				<tr>
					<td><label for="Matri">*Matrícula </label> <input type="text" name="matricula" id="Matri" maxlength="10" pattern=".{10,11}" class="campo" required pattern='[0-9]+$' placeholder="apenas numeros"></td>
					<td>*Data de Nascimento <input type="date" name="nasc" id="cNasc" class="campo" required pattern="[0-9]{2}\/[0-9]{2}\/[0-9]{4}$"></td>
					<td>*Sexo<br><input type="radio" name="sexo" value="Masculino" id="masc" required><label>Masculino</label><br><input type="radio" name="sexo" value="Feminino" id="fem" required><label>Feminino</label>
						<br><input type="radio" name="sexo" value="Outro" required><label>Outro</label></td>
					<td></td>
				</tr>
				<tr>
					<td><label for="cEnd">*Endereço </label><input type="text" name="end" id="cEnd" maxlength="150" class="campo" required></td>
					<td><label for="cEmail">*E-mail </label><input type="email" name="email" id="cEmail" maxlength="80" class="campo" required></td>
					<td><br><label for="cSenha" >*Senha </label><input type="password" name="senha" id="cSenha" pattern=".{8,20}" maxlength="20" class="campo" required placeholder="senha com no mínino 8 caracteres" ><br><span class="style1">*Campos com * são obrigatorios</span></td>
				</tr>
				<tr>
					<td>*Titulação <select name="titulacao" class="campo" required>
				<option value="Doutor">Doutor</option>
				<option value="Livre-Docente">Livre-Docente</option>
				<option value="Assistente">Assistente</option>
				<option value="Titular">Titular</option>
				<option value="Presidente">Presidente</option>
				<option value="Vice-Presidente">Vice-Presidente</option>       
				</select></td>
					<td><a href="cadastrar.php"><input class="botao salv" type="submit" name="Salvar" value="Salvar"></a> 
					<a href="login.php"><input class="botao canc" type="button" name="Cancelar" value="Cancelar"></a> </td>
				</tr>
			</thead>

		</table>
  
	</form>


	<div class="footer" align="left">Desenvolvido por Glenda Mahayana</div>

</body>
</html>