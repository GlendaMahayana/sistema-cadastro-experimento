<?php
include_once("conexao.php");
@session_start();

$cod = $_SESSION['cod'];
$email = $_SESSION['email'];
$nome = $_SESSION['nome'];

  if (!isset($_SESSION['nome']) && !isset($_SESSION['email']) && !isset($_SESSION['cod'])) {

    header('Location: cadastro_exp.php');
                
          exit;  
  }

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Cadastro Experimento</title>
	<link rel="stylesheet" type="text/css" href="css/cadastro_exp.css">

</head>
<body>
  <form method="POST" action="cadastrar_exp.php">

  		<div class="horinzontal">

  			<img src="css/imagens/logo_marca.png"  width="100px" id="logo">
  			<div id="sistema"><br>SISTEMA ANIMAL</div>
  			
  		</div>
			
		<table>
			<caption>Cadastro de Experimento</caption>
			<thead>
				<tr>
					<td>*Justificativa para uso<textarea name="justificar_decisao"  class="campo" required></textarea><a href=""><input class="botao anex" type="submit" name="Anexar" value="Anexar arquivo"></a></td>
					<td>*Resumo<textarea name="resumo" class="campo" required></textarea><a href=""><input class="botao anex" type="submit" name="Anexar" value="Anexar arquivo" ></a></td>
					<td>*Data de Inicio <input type="date" name="inicio" id="Inicio" class="campo" required maxlength="10" pattern="[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" min="2018-06-01">
					<td>*Data de Término <input type="date" name="termino" id="Término" class="campo" required="required" maxlength="10" pattern="[0-9]{2}\/[0-9]{2}\/[0-9]{4}$" min="2018-01-01"></td></td>
				</tr>
				<tr>
					<td>*Especie<select name="especie" class="campo" required>
						<option value="Rato">Rato</option>
						<option value="Coelho">Coelho</option>
						<option value="Porquinho-da-índia">Porquinho-da-índia</option>
						<option value="Cão">Cão</option>
						<option value="Rã">Rã</option>
						<option value="Peixe">Peixe</option>
						</select></td>
					<td>*Origem<select name="origem" class="campo" required>
						<option value="Bioterio1">Bioterio1</option>
						<option value="Bioterio2">Bioterio2</option>
						<option value="Bioterio3">Bioterio3</option>
						<option value="Bioterio4">Bioterio4</option>
						<option value="Bioterio5">Bioterio5</option>
						<option value="Bioterio6">Bioterio6</option>
						</select></td>
					<td>*Quantidade<select name="quantidade" class="campo" required>
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
						<option value="6">6</option>
						</select></td>
				</tr>
				<tr>
					<td></td>
					<td><a href=""><input class="botao prot" type="submit" name="Gerar Protocolo" value="Gerar Protocolo"></a>
					<a href="menu.php"><input class="botao prot" type="button" name="Cancelar" value="Cancelar"></a></td>
				</tr>
			</thead>

		</table>
  		
	</form>

<div class="footer" align="right">Desenvolvido por Glenda Mahayana</div>
</body>
</html>